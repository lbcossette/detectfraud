import math

daysOfWeek = {"Mon" : 0, "Tue" : 1, "Wed" : 2, "Thu" : 3, "Fri" : 4, "Sat" : 5, "Sun" : 6}

monthsOfYear = {"Jan" : 1, "Feb" : 2, "Mar" : 3, "Apr" : 4, "May" : 5, "Jun" : 6, "Jul" : 7, "Aug" : 8, "Sep" : 9, "Oct" : 10, "Nov" : 11, "Dec" : 12}

# Julian Day Number calculation
def julianDayNumber(year, month, day):
    # From :
    # https://en.wikipedia.org/wiki/Julian_day#Converting_Gregorian_calendar_date_to_Julian_Day_Number
    # (L. E. Doggett, Ch. 12, "Calendars", p. 604, in Seidelmann 1992)
    return int((1461*(year + 4800 + int((month - 14)/12)))/4) + int((367*(month - 2 - 12*int((month - 14)/12)))/12) - int((3*int(((year + 4900 + int((month - 14)/12))/100)))/4) + day - 32075

# Circle of the day
def circleOfDay(hour, minute):
    return [math.cos((60*hour + minute)*math.pi/720),math.sin((60*hour + minute)*math.pi/720)]

# Circle of the week
def circleOfWeek(day, hour):
    return [math.cos((24*day + hour)*math.pi/84),math.sin((24*day + hour)*math.pi/84)]

def secondsOfDay(sec):
    return [math.cos(int(sec/60)*math.pi/720),math.sin(int(sec/60)*math.pi/720)]

def secondsOfWeek(sec):
    return [math.cos(int(sec/3600)*math.pi/84),math.sin(int(sec/3600)*math.pi/84)]

yearJulDayNums = dict()

# Circle of the year
def circleOfYear(julDayNum, year):
    if not (year in yearJulDayNums and year+1 in yearJulDayNums):
        yearJulDayNums[year] = julianDayNumber(year,1,1)
        yearJulDayNums[year+1] = julianDayNumber(year+1,1,1)
    
    return [math.cos((julDayNum - yearJulDayNums[year])*2*math.pi/(yearJulDayNums[year+1] - yearJulDayNums[year]))]