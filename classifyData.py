import numpy as np
import pandas as pd
#from sklearn.mixture import GaussianMixture
#from copy import deepcopy
from sklearn.svm import LinearSVC, SVC
import math
from random import randint

# Read processed data csv
# i.e. please run processData.py before this script
df = pd.read_csv("processedData.csv")

positives = df.loc[df["label"] == 1]
negatives = df.loc[df["label"] == 0]

allPos = len(positives.index)
#print(allPos)

allNeg = len(negatives.index)
#print(allNeg)

negativesBalanced = negatives.iloc[0].to_dict()

for key in negativesBalanced:
    negativesBalanced[key] = []

n = 0

# Create balanced dataset
for i in range(allNeg):
    #if i % 1000 == 0:
        #print(i)
    if randint(1, allNeg) <= allPos:
        n += 1
        entry = negatives.iloc[i].to_dict()

        for key in entry:
            negativesBalanced[key].append(entry[key])

# Create a randomly undersampled balanced dataset
balanced = pd.DataFrame(negativesBalanced).append(positives)

cats = balanced.loc[:,"label"].values
vals = balanced.loc[:,"dayCos":].values
allPosCats = positives.loc[:,"label"].values
allPosVals = positives.loc[:,"dayCos":].values
allNegCats = negatives.loc[:,"label"].values
allNegVals = negatives.loc[:,"dayCos":].values

linearClassifier = LinearSVC(dual = False)
linearClassifier.fit(vals,cats)

print("Linear classification")
print("-------------------------------------------------------")
print("Coefficients :")
print(linearClassifier.coef_)
print("Recall (power) :")
print(linearClassifier.score(allPosVals,allPosCats))
print("True negative rate :")
print(linearClassifier.score(allNegVals,allNegCats))

"""
# The RBF classifier is not so good here
rbfClassifier = SVC()
rbfClassifier.fit(vals,cats)

print("RBF kernel classification")
print("-------------------------------------------------------")
print("Support vectors :")
print(rbfClassifier.support_vectors_)
print("Recall (power) :")
print(rbfClassifier.score(allPosVals,allPosCats))
print("True negative rate :")
print(rbfClassifier.score(allNegVals,allNegCats))
"""

poly2Classifier = SVC(kernel="poly", degree = 2)
poly2Classifier.fit(vals,cats)

print("Polynomial kernel classification, degree 2")
print("-------------------------------------------------------")
print("Support vectors :")
print(poly2Classifier.support_vectors_.shape)
print("Recall (power) :")
print(poly2Classifier.score(allPosVals,allPosCats))
print("True negative rate :")
print(poly2Classifier.score(allNegVals,allNegCats))

poly3Classifier = SVC(kernel="poly")
poly3Classifier.fit(vals,cats)

print("Polynomial kernel classification, degree 3")
print("-------------------------------------------------------")
print("Support vectors :")
print(poly3Classifier.support_vectors_.shape)
print("Recall (power) :")
print(poly3Classifier.score(allPosVals,allPosCats))
print("True negative rate :")
print(poly3Classifier.score(allNegVals,allNegCats))

poly4Classifier = SVC(kernel="poly", degree = 4)
poly4Classifier.fit(vals,cats)

print("Polynomial kernel classification, degree 4")
print("-------------------------------------------------------")
print("Support vectors :")
print(poly4Classifier.support_vectors_.shape)
print("Recall (power) :")
print(poly4Classifier.score(allPosVals,allPosCats))
print("True negative rate :")
print(poly4Classifier.score(allNegVals,allNegCats))

# Old code using gaussian mixture to analyze data
"""
comps = []

for i in range(1,29):
    comps.append("V{:n}".format(i))

# Get a hold of what the data contains, and possible patterns

# One way to solve this problem is to use a Gaussian Mixture Model
# to build classes for positives and negatives

# Large variance differences between positives and negatives
# Simple check using Bharracharyya distance classification error estimation
# Error = 40.219 − 70.019*b + 63.578*b^2 − 32.766*b^3+ 8.7172*b^4 − 0.91875*b^5
# b is Bhattacharyya distance
# For a 5% expected classification error, b >= 1.47
# For a 2.5% expected classification error, b >= 1.98
# For a 1% expected classification error, b >= 2.82
# For a 0% expected classification error, b >= 3.1
# Obviously, this is an estimate, but knowing which components
# are the most useful for classification is helpful in itself.

# Ref : Choi E, Lee C. Feature extraction based on the Bhattacharyya distance.
# 2003. Pattern Recognition. Vol 36. p1703-1709
deltaMean = positives.mean().values - negatives.mean().values
var1 = positives.var().values
var2 = negatives.var().values
var12 = np.divide(var1 + var2, 2)

term1 = np.divide(deltaMean * deltaMean * var12, 8)
term2 = np.divide(np.log(var12 / np.sqrt(var1 * var2)), 2)

bhat = term1 + term2

# Time cannot be analyzed using this method, but one can see that
# components 1-5, 7, 9-12, 14 and 16-18 should be sufficient for
# classification
# This is a crude estimate, however, so we will process some more

# print(bhat)

# Gaussian mixture
def gaussianMixtureFit(data, nTries):
    allModels = []
    allModelScores = []
    nClus = 1

    while len(allModels) < 3 or allModelScores[0] - allModelScores[-1] > 1.05*(allModelScores[0] - allModelScores[-2]):
        models = []

        # Fit done in triplicate
        for i in range(nTries):
            mix = GaussianMixture(n_components=nClus, covariance_type="diag", verbose=True)
            mix.fit(data)
            models.append(deepcopy(mix))

        bestModel = models[0]
        bestModelScore = models[0].bic(data)
        for i in range(1,nTries):
            if models[i].bic(data) < bestModelScore:
                bestModel = models[i]
                bestModelScore = models[i].bic(data)

        allModels.append(bestModel)
        allModelScores.append(bestModelScore)
        print(allModelScores)
        nClus += 1
    
    return allModels, allModelScores

goodComps = ["V1","V2","V3","V4","V5","V7","V9","V10","V11","V12","V14","V16","V17","V18"]

posComps = positives.loc[:,goodComps].values
posModels, posModelScores = gaussianMixtureFit(posComps, 10)
bestPosModel = posModelScores.index(min(posModelScores))

negComps = negatives.loc[:,goodComps].values
negModels, negModelScores = gaussianMixtureFit(negComps, 10)
bestNegModel = negModelScores.index(min(negModelScores))

def appendToDict(dicti, keys, values):
    for i in range(len(keys)):
        dicti[keys[i]].append(values[i])

posMeans = deepcopy(posModels[bestPosModel].means_)
posCovs = deepcopy(posModels[bestPosModel].covariances_)
posWeights = deepcopy(posModels[bestPosModel].weights_)
negMeans = deepcopy(negModels[bestNegModel].means_)
negCovs = deepcopy(negModels[bestNegModel].covariances_)
negWeights = deepcopy(negModels[bestNegModel].weights_)

modelDict = {"class" : [], "weight" : [],
                "V1_mean" : [],"V2_mean" : [],"V3_mean" : [],
                "V4_mean" : [],"V5_mean" : [],"V7_mean" : [],
                "V9_mean" : [],"V10_mean" : [],"V11_mean" : [],
                "V12_mean" : [],"V14_mean" : [],"V16_mean" : [],
                "V17_mean" : [],"V18_mean" : [],
                "V1_var" : [],"V2_var" : [],"V3_var" : [],
                "V4_var" : [],"V5_var" : [],"V7_var" : [],
                "V9_var" : [],"V10_var" : [],"V11_var" : [],
                "V12_var" : [], "V14_var" : [],"V16_var" : [],
                "V17_var" : [],"V18_var" : []}

meanKeys = ["V1_mean","V2_mean","V3_mean",
                "V4_mean","V5_mean","V7_mean",
                "V9_mean","V10_mean","V11_mean",
                "V12_mean","V14_mean","V16_mean",
                "V17_mean","V18_mean"]

varKeys = ["V1_var","V2_var","V3_var",
                "V4_var","V5_var","V7_var",
                "V9_var","V10_var","V11_var",
                "V12_var", "V14_var","V16_var",
                "V17_var","V18_var"]

for i in range(len(posMeans)):
    modelDict["class"].append(1)
    modelDict["weight"].append(posWeights[i])
    appendToDict(modelDict, meanKeys, posMeans[i])
    appendToDict(modelDict, varKeys, posCovs[i])

for i in range(len(negMeans)):
    modelDict["class"].append(0)
    modelDict["weight"].append(negWeights[i])
    appendToDict(modelDict, meanKeys, negMeans[i])
    appendToDict(modelDict, varKeys, negCovs[i])

mdl = pd.DataFrame(modelDict)
mdl.to_csv("model.csv")

# TODO :
# The objective is to create one or several polytopes around the negative classes.
# We will use the normal distributions to locate the hyperplanes' optimal
# position and orientation.


# Some extra analysis of the model

def bharracharyyaSeparate(mean1,var1,mean2,var2):
    # Returns a vector containing the contribution
    # of each dimension to the overall bhattacharyya distance
    # Only valid if covariance matrices are both diagonal
    if len(mean1) != len(mean2):
        raise Exception("Both distributions must have the same dimensionality")

    contribs = []

    for i in range(len(mean1)):
        term = (var1[i] + var2[1])/16*(mean1[i]-mean2[i])**2 + math.log((var1[i] + var2[1])/2/math.sqrt(var1[i]*var2[1]))/2
        contribs.append(term)

    return contribs

def bharracharyya(mean1,var1,mean2,var2):
    # Returns the Bhattacharyya distance between two distributions
    # Currently only valid for diagonal covariance matrices
    if len(mean1) != len(mean2):
        raise Exception("Both distributions must have the same dimensionality")

    dist = 0.0

    for i in range(len(mean1)):
        dist += (var1[i] + var2[1])/16*(mean1[i]-mean2[i])**2 + math.log((var1[i] + var2[1])/2/math.sqrt(var1[i]*var2[1]))/2

    return dist


contribDict = {"positive_class" : [], "negative_class" : [], "V1" : [],"V2" : [],"V3" : [],
                "V4" : [],"V5" : [],"V7" : [],"V9" : [],"V10" : [],"V11" : [],"V12" : [],
                "V14" : [],"V16" : [],"V17" : [],"V18" : []}

for i in range(len(posMeans)):
    for j in range(len(negMeans)):
        contribDict["positive_class"].append(i)
        contribDict["negative_class"].append(j)
        contribs = bharracharyyaSeparate(posMeans[i],posCovs[i],negMeans[j],negCovs[j])
        appendToDict(contribDict, goodComps, contribs)

summ = pd.DataFrame(contribDict)

summ.to_csv("modelSeps.csv", index=False)
"""