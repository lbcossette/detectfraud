#import tensorflow as tf
#from tensorflow import keras
from re import split, sub
from wheelOfTime import secondsOfDay, secondsOfWeek
import numpy as np
from random import randint
import pandas as pd
import sklearn.preprocessing as prep

# Read dataset CSV
# Found at https://www.kaggle.com/mlg-ulb/creditcardfraud
with open("creditcard.csv", "r") as rf:
    allLines = rf.readlines()

    rf.close()

# Some preprocessing
labels = []
timeData = []
components = []
amount = []

for i in range(1,len(allLines)):
    allLines[i] = split(",", sub('"', "", allLines[i].rstrip()))

    # Assign time to the circle of the day and the circle of the week
    # We do not know the exact time, but placing it on a wheel gives
    # the relative time 
    # When scientific notation is used, we have to use float
    timeData.append(secondsOfDay(int(float(allLines[i][0]))))
    timeData[-1].extend(secondsOfWeek(int(float(allLines[i][0]))))
    components.append(list(map(lambda x : float(x), allLines[i][1:-2])))
    amount.append(float(allLines[i][-2]))

    labels.append(int(allLines[i][-1]))

# Create single 2D array with all data
timeData = np.transpose(np.array(timeData))
components = np.transpose(np.array(components))

# Create dataframe and save to csv
dataDict = dict()

dataDict["label"] = labels
dataDict["dayCos"] = timeData[0]
dataDict["daySin"] = timeData[1]
dataDict["weekCos"] = timeData[2]
dataDict["weekSin"] = timeData[3]

for i in range(28):
    dataDict["V{:n}".format(i+1)] = components[i]

robScale = prep.RobustScaler()

dataDict["amount"] = amount

df = pd.DataFrame(dataDict)

# Rescale transaction amount data using median and interquartile range
df['amount'] = robScale.fit_transform(df['amount'].values.reshape(-1,1))

# Save data to CSV
df.to_csv("processedData.csv", index=False)